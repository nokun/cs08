<?php

$host = "localhost";
$user = "root";
$password = "";
$db = 'assignment05';

$conn = mysqli_connect($host,$user,$password,$db) or die("unable to connect");
$query = "SELECT students.name, students.surname, courses.name, grades.grade, grades.student_id, grades.course_id, students.id, courses.id FROM students, grades, courses WHERE grade > 26 AND students.id = grades.student_id AND courses.id = grades.course_id";
$students =  mysqli_query($conn, $query);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Students</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Course</th>
                <th>Grade</th>
            </tr>
        </thead>
        <tbody>
        <?php while($student = mysqli_fetch_array($students)) { ?>
            <tr>
                <td><?= $student['0'] ?></td>
                <td><?= $student['1'] ?></td>
                <td><?= $student['2'] ?></td>
                <td><?= $student['3'] ?></td>
            </tr>

        <?php } 
        mysqli_close($conn) ?>
        </tbody>
    </table>
</body>
</html>