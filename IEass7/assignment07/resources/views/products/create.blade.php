@extends('products.layout')

@section('content')


<form action="{{ route('products.store') }}" method="POST">
@csrf
<input placeholder="Name" class="form-control" type="text" name="name">
<input placeholder="Price" class="form-control" type="number" step="0.01" name="price">
<input type="submit" class="btn btn-primary">
</form>
