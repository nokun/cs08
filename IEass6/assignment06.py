from flask import Flask, request
from flask import  render_template
#step2
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
#Step3
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.assignment06'

#step4
db = SQLAlchemy(app)

#####

#TODO add Product class (model and __init__)

class Product(db.Model):
    id = db.Column('id', db.Integer, primary_key = True)
    name = db.Column('name', db.String(30))
    price = db.Column('price', db.Float)

    def __init__(self, name, price):
        self.name = name
        self.price = price

#####

# @app.route('/welcome/<name>')

# @app.route('/')

@app.route('/newpdt')
def new_product():
   return render_template('add_product.html')


@app.route('/addpdt1',methods = ['POST'])
def addstd1():

	#TODO1 read POST pars 	
	if request.method == 'POST':
		name=request.form['name']
		price=request.form['price']

		#TODO2 returns product.html with the product information
		msg = 'Product received correctly!'
		
	return render_template("product.html", msg = msg, name = name, price = price)


@app.route('/addpdt2',methods = ['POST'])
def addpdt2():

	#TODO1 read POST pars 	
	name = request.form['name']
	price = request.form['price']
	
	#TODO2 create Product and add to the DB
	product = Product(name, price)
	db.session.add(product)
	db.session.commit()
	#TODO3 returns product.html with the product information

	return render_template("products.html", products = Product.query.all())
	

@app.route('/list')
def list():
   return render_template("products.html", products = Product.query.all())

@app.route('/assignment06/<name>')
def welcome(name):
	return render_template("welcome.html", name = name)

@app.route('/')
def index():
	return render_template("index.html")


if __name__ == '__main__':
	#step5
	db.create_all()
	app.run(debug = True)




